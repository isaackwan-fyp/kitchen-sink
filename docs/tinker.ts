const Web3 = require('web3');
const web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));

import CONSTANTS from '../src/app/services/registry.constants';
web3.eth.defaultAccount = "0xb61538ee21a333b13e37e9f8e7354de0795b96af"

var MyContract = web3.eth.contract(CONSTANTS.SPEC.abi);
var myContractInstance = MyContract.at("0x31a178b012821cbda30f17832f30b93ca95617c6");
var result = myContractInstance.register({gas: "6721975"}); // {gas: "6721975"}
console.log(result)

const event = myContractInstance.Registerd([{_from: web3.eth.defaultAccount}]);
event.watch(function(error: any, result: any){
	console.log("err", error);
	console.log("result", result, result.args.id.toString());
	event.stopWatching();
  });
  