export interface Avatar {
	uri: string,
}

export default interface User {
	name: string,
	address: string,
	from: {network: string, address: string},
	avatar?: Avatar,
}