import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegistryMutationsComponent } from './components/registry-mutations/registry-mutations.component';
import { WelcomePageComponent } from './components/welcome-page/welcome-page.component';
import { ClaimMakeComponent } from './components/claim-make/claim-make.component';
import { ClaimViewComponent } from './components/claim-view/claim-view.component';
import { AssertPageComponent } from './components/assert-page/assert-page.component';
import { VerifyPageComponent } from './components/verify-page/verify-page.component';
import { FileAssertsComponent } from './components/file-asserts/file-asserts.component';
import { FileAsserts2Component } from './components/file-asserts2/file-asserts2.component';
import { FileNewComponent } from './components/file-new/file-new.component';
import { Asserts2OfUserComponent } from './components/asserts2-of-user/asserts2-of-user.component';
import { VoteCreateComponent } from './components/vote-create/vote-create.component';
import { CompetitionDetailComponent } from './components/competition-detail/competition-detail.component';

const routes: Routes = [
  { path: '', component: WelcomePageComponent },
  { path: 'registry', component: RegistryMutationsComponent },
  { path: 'claim-make', component: ClaimMakeComponent },
  { path: 'claim-view', component: ClaimViewComponent },
  { path: 'assert-page', component: AssertPageComponent },
  { path: 'verify-page', component: VerifyPageComponent },
  { path: 'file-asserts', component: FileAssertsComponent },
  { path: 'file-asserts2', component: FileAsserts2Component },
  { path: 'file-asserts2/:checksum/:fileExtension', component: FileAsserts2Component },
  { path: 'file-new', component: FileNewComponent },
  { path: 'users2/:userId', component: Asserts2OfUserComponent },
  { path: 'competitions', component: VoteCreateComponent },
  { path: 'competitions/:competitionAddress', component: CompetitionDetailComponent },
]

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ],
})
export class AppRoutingModule {}