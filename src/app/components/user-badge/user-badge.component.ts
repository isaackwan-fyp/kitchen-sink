import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { RegistryService } from '../../services/registry.service';

@Component({
  selector: 'app-user-badge',
  templateUrl: './user-badge.component.html',
  styleUrls: ['./user-badge.component.css']
})
export class UserBadgeComponent implements OnInit {
  @Input()
  address: string;

  @Input()
  name: string;

  @Input()
  avatar: string = "/assets/icon_loading.svg";

  pagerank: string | number;

  constructor(private registryService: RegistryService) { }

  ngOnInit() {
    if (this.address) {
      this.registryService.getName(this.address)
        .then((name) => {this.name = name});
      
      this.registryService.pagerank(this.address)
        .then((pagerank) => {this.pagerank = Math.round(parseInt(pagerank, 10) / 10000) });

      this.registryService.getAvatar(this.address)
        .then((avatar) => {this.avatar = avatar || "/assets/icon_user.svg"});
    }
  }

  ngOnChanges() {
    console.log("changes detected");
    this.ngOnInit();
  }

  get link() {
    if (this.address) {
      return `/users2/${this.address}`;
    } else {
      return "#";
    }
  }

}
