import { Component, OnInit } from '@angular/core';
import sha1 from "js-sha1";
import { get, last } from "lodash";

interface File extends Blob {
  readonly lastModified: number;
  /** @deprecated */
  readonly lastModifiedDate: Date;
  readonly name: string;
  readonly webkitRelativePath: string;
}

function checksumForFile(file: File): Promise<string> {
  return new Promise((resolve, reject) => {
    var reader = new FileReader();
    reader.onload = function (event: any) {
      resolve(sha1(event.target.result));
    };
    reader.onerror = function (event: any) {
      reject(event);
    }
    reader.readAsArrayBuffer(file);  
  });
}

@Component({
  selector: 'app-file-new',
  templateUrl: './file-new.component.html',
  styleUrls: ['./file-new.component.css']
})
export class FileNewComponent implements OnInit {

  canUpload: boolean = true;
  checksum: string = "";
  file: File;
  constructor() { }

  ngOnInit() {
  }

  async generateChecksum(event: any) {
    if (!event.target.files[0]) {
      throw new Error("No file is selected");
    }
    this.file = event.target.files[0];
    this.canUpload = false;
    const checksum = await checksumForFile(this.file);
    console.log("checksum", checksum);
    this.checksum = checksum;
  }

  get filePage() {
    if (!get(this, "file.name")) {
      return "";
    }
    return `/file-asserts2/${this.checksum}/${last(get(this, "file.name").split("."))}`;
  }

  get fileLastModified() {
    if (!this.file) {
      return undefined;
    }
    return new Date(this.file.lastModified);
  }

}
