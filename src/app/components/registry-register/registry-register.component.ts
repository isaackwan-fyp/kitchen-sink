import { Component, ViewEncapsulation } from '@angular/core';
import { RegistryService } from '../../services/registry.service';

@Component({
  selector: 'app-registry-register',
  templateUrl: './registry-register.component.html',
  styleUrls: ['./registry-register.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class RegistryRegisterComponent {
  address1: string;
  address2: string;
  result: string;

  constructor(private registryService: RegistryService) { }

  register() {
    this.result = "loading...";
    return this.registryService.register(this.address1, this.address2).then((rtn) => {this.result = `ID = ${rtn}`});
  }

  friend() {
    this.result = "loading...";
    return this.registryService.friend(this.address1).then(() => {this.result = 'done!'});
  }

  unfriend() {
    this.result = "loading...";
    return this.registryService.unfriend(this.address1).then(() => {this.result = 'done!'});
  }

  isFriend() {
    this.result = "loading...";
    return this.registryService.isFriend(this.address1, this.address2).then((rtn) => {this.result = rtn.toString()});
  }

  pagerank() {
    this.result = "loading...";
    return this.registryService.pagerank(this.address1).then((rtn) => {this.result = rtn.toString()});
  }

}
