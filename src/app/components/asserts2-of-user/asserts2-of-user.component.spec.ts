import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Asserts2OfUserComponent } from './asserts2-of-user.component';

describe('Asserts2OfUserComponent', () => {
  let component: Asserts2OfUserComponent;
  let fixture: ComponentFixture<Asserts2OfUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Asserts2OfUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Asserts2OfUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
