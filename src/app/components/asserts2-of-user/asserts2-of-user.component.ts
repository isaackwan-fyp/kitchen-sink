import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { FilesService } from '../../services/files.service';
import { RegistryService } from '../../services/registry.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Web3Service } from '../../services/web3.service';
import { combineLatest } from 'rxjs/observable/combineLatest';

@Component({
  selector: 'app-asserts2-of-user',
  templateUrl: './asserts2-of-user.component.html',
  styleUrls: ['./asserts2-of-user.component.css']
})
export class Asserts2OfUserComponent implements OnInit {
  address: string = "0xload";
  name: string = "Loading...";
  claims = <any[]>[];
  loading: boolean = false;
  avatar: string = "";
  pagerank: string | number;
  isFriend: boolean | undefined;

  constructor(private route: ActivatedRoute, private filesService: FilesService, private registryService: RegistryService, private modalService: NgbModal, private web3Service: Web3Service) { }

  open(content: any) {
    this.modalService.open(content);
  }

  async friend(e: any) {
    e.target.disabled = true;
    await this.registryService.friend(this.address);
    e.target.disabled = false;
    this.isFriend = true;
  }

  async unfriend(e: any) {
    e.target.disabled = true;
    await this.registryService.unfriend(this.address);
    e.target.disabled = false;
    this.isFriend = false;
  }

  ngOnInit() {
    this.route.paramMap.subscribe(async (params) => {
      const address = params.get("userId");

      if (!address) {
        console.log("Not loading resources for user because address is not available");
        return;
      }

      this.loading = true;
      this.claims = [];

      this.address = address;
      const loadName = this.registryService.getName(address).then((name) => {this.name = name});
      const loadAvatar = this.registryService.getAvatar(address).then((avatar) => {
        this.avatar = avatar ? avatar : "/assets/icon_user.svg";
      });
      this.registryService.pagerank(this.address)
        .then((pagerank) => {this.pagerank = Math.round(parseInt(pagerank, 10) / 10000) });
  
      for await (const claim of this.filesService.getClaimsByUser(address)) {
        const key_object: any = JSON.parse(claim.key);
        claim.key_decoded = `${key_object.checksum}.${key_object.fileExtension}`;
        claim.link = `/file-asserts2/${key_object.checksum}/${key_object.fileExtension}`;

        console.log("claim", claim, key_object);
        this.claims.push(claim);
      }

      await Promise.all([loadName, loadAvatar]);
      this.loading = false;
    });

    combineLatest(this.web3Service.address, this.route.paramMap).subscribe(async ([myAddress, params]) => {
      const hisAddress = params.get("userId");
      if (!hisAddress || hisAddress === myAddress) {
        console.log("NOTE: this is current user's page");
        this.isFriend = undefined;
        return;
      }
      this.isFriend = await this.registryService.isFriend(myAddress, hisAddress);
    });
    
  }

}
