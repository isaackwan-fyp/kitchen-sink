import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-assert-page',
  templateUrl: './assert-page.component.html',
  styleUrls: ['./assert-page.component.css']
})
export class AssertPageComponent implements OnInit {
  url: any = this.sanitizer.bypassSecurityTrustResourceUrl("https://lite.cnn.io/en");
  urlInput: string = "https://lite.cnn.io/en";
  previewUrl: any = "http://localhost:4201/screenshot?width=600&url=http://www.ip6.me";
  @ViewChild('preview')
  preview: ElementRef;

  constructor(private sanitizer: DomSanitizer) { }

  ngOnInit() {
  }

  loadPreview() {
    this.url = this.sanitizer.bypassSecurityTrustResourceUrl(this.urlInput);
    this.previewUrl = this.sanitizer.bypassSecurityTrustResourceUrl(`http://localhost:4201/screenshot?width=${this.preview.nativeElement.offsetWidth}&url=${encodeURIComponent(this.urlInput)}`);
  }

  get urlSafe() {
    return this.sanitizer.bypassSecurityTrustResourceUrl(this.url);
  }

}
