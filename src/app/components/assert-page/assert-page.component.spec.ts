import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssertPageComponent } from './assert-page.component';

describe('AssertPageComponent', () => {
  let component: AssertPageComponent;
  let fixture: ComponentFixture<AssertPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssertPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssertPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
