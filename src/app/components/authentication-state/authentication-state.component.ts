import { Component, OnInit } from '@angular/core';
import { Web3Service } from '../../services/web3.service'

@Component({
  selector: 'app-authentication-state',
  templateUrl: './authentication-state.component.html',
  styleUrls: ['./authentication-state.component.css'],
})
export class AuthenticationStateComponent implements OnInit {

  address?: string;

  constructor(private web3Service: Web3Service) { }

  ngOnInit() {
    this.web3Service.address.subscribe((address) => {
      this.address = address;
    });
  }

  switchToNormal() {
    return this.web3Service.switchToNormal();
  }

  switchToUport() {
    return this.web3Service.switchToUport();
  }

}
