import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { MakeCompetitionService } from '../../services/make-competition.service';
import { ViewCompetitionService } from '../../services/view-competition.service';

export interface Contestant {
  file: string, // YouTube or Image link
  description: string,
}

@Component({
  selector: 'app-vote-create',
  templateUrl: './vote-create.component.html',
  styleUrls: ['./vote-create.component.css']
})
export class VoteCreateComponent implements OnInit {
  images: Array<string>;
  newContestant: Contestant = {file: "", description: ""};
  contestants: Contestant[] = [];
  payout: string = "0";
  name: string;
  competitions: any[] = [];
  loading: boolean = true;

  constructor(private makeCompetitionService: MakeCompetitionService, private viewCompetitionService: ViewCompetitionService, private router: Router) {}

  async ngOnInit() {
    this.loading = true;
    const promises = [];

    for await (const competition of this.makeCompetitionService.getCompetitionAddresses()) {
      promises.push(this.viewCompetitionService.getCompetitionDetails(competition).then((info: any) => {
        this.competitions.push(info);
      }));
    }
    Promise.all(promises).then(() => {
      this.loading = false;
    });
  }

  addContestant() {
    this.contestants.push(this.newContestant);
    this.newContestant = {file: "", description: ""};
  }

  async createCompetition(event: any) {
    event.target.disabled = true;
    const competitionRegistry = await this.makeCompetitionService.create(this.name, this.contestants, parseInt(this.payout, 10));
    this.router.navigate(['/competitions', competitionRegistry]);
  }

}
