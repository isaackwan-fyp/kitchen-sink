import { Component } from '@angular/core';
import { MakeClaimService } from '../../services/make-claim.service';

@Component({
	selector: 'app-claim-make',
	templateUrl: './claim-make.component.html',
	styleUrls: ['./claim-make.component.css']
})
export class ClaimMakeComponent {
	arg1: string;
	arg2: string;
	arg3: string = "0";
	result: string;

	constructor(private makeClaimService: MakeClaimService) { }

	claim() {
		this.result = "Loading...";
		this.makeClaimService.claim(this.arg1, this.arg2, parseInt(this.arg3, 10)).then((address) => {
			this.result = address;
		});
	}

	async getClaims() {
		this.result = "";
		const claims = await this.makeClaimService.getClaims();
		for await (const claim of claims) {
			this.result += `${claim} `;
		}
	}
}
