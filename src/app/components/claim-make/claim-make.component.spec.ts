import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClaimMakeComponent } from './claim-make.component';

describe('ClaimMakeComponent', () => {
  let component: ClaimMakeComponent;
  let fixture: ComponentFixture<ClaimMakeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClaimMakeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClaimMakeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
