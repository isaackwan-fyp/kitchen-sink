import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-file-asserts',
  templateUrl: './file-asserts.component.html',
  styleUrls: ['./file-asserts.component.css']
})
export class FileAssertsComponent implements OnInit {
  filename: string;
  size: string;
  type: string;
  showFileInfo: boolean = false;
  @Output() public dragFileAccepted: EventEmitter<Object> = new EventEmitter();
  public isHovering: boolean = false;
  constructor() {
  }

  ngOnInit() {
    this.dragFileAccepted.subscribe((files: any) => {
      this.showFileInfo = true;
      this.filename = files.file.name;
      this.size = files.file.size;
      this.type = files.file.type;
    });
  }

  onDragFileOverStart(event: any) {
    if (!this.isHovering) {
      this.isHovering = true;
    }
    this.preventDefaultAndStopPropagation(event);
    return false;
  };

  onDragFileOverEnd(event: any): any {
    this.preventDefaultAndStopPropagation(event);
    // console.log(event);
    return false;
  }

  onDragFileAccepted(acceptedFile: File): any {
      if (this.dragFileAccepted) {
        this.dragFileAccepted.emit({ file: acceptedFile });
      }
  }

  onDragFileDrop(event: any): any {
    this.preventDefaultAndStopPropagation(event);
    this.FileSelectHandler(event);
  }

  preventDefaultAndStopPropagation(event: any) {
    event.preventDefault();
    event.stopPropagation();
  }

  public FileSelectHandler(e: any) {
    this.isHovering = false;      // cancel the hover
    var files = e.target.files || e.dataTransfer.files;     // fetch FileList object

    // process all File objects
    for (var i = 0, f; f = files[i]; i++) {
      this.onDragFileAccepted(f);
    }
  }

}
