import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FileAssertsComponent } from './file-asserts.component';

describe('FileAssertsComponent', () => {
  let component: FileAssertsComponent;
  let fixture: ComponentFixture<FileAssertsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FileAssertsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileAssertsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
