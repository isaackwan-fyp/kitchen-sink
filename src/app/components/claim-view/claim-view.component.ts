import { Component } from '@angular/core';
import { ViewClaimService } from '../../services/view-claim.service';

@Component({
	selector: 'app-claim-view',
	templateUrl: './claim-view.component.html',
	styleUrls: ['./claim-view.component.css']
})
export class ClaimViewComponent {
	arg1: string;
	result: string;

	constructor(private viewClaimService: ViewClaimService) { }

	async getClaim() {
		// this.result = "Not Implemented...";
		this.result = JSON.stringify(await this.viewClaimService.getClaimByAddress(this.arg1), null, 2);
	}

	voteUp() {
		this.result = "Loading...";
		this.viewClaimService.voteUp(this.arg1)
			.then((result) => "reward = " + result)
			.then(this._justPrint.bind(this), this._justPrint.bind(this));
			
	}

	voteDown() {
		this.result = "Loading...";
		this.viewClaimService.voteDown(this.arg1)
			.then((result) => "reward = " + result)
			.then(this._justPrint.bind(this), this._justPrint.bind(this));
			
	}

	_justPrint(value: any) {
		this.result = value;
	}
}
