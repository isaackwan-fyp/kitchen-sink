import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistryMutationsComponent } from './registry-mutations.component';

describe('RegistryMutationsComponent', () => {
  let component: RegistryMutationsComponent;
  let fixture: ComponentFixture<RegistryMutationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistryMutationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistryMutationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
