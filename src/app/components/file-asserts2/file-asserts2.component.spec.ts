import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FileAsserts2Component } from './file-asserts2.component';

describe('FileAsserts2Component', () => {
  let component: FileAsserts2Component;
  let fixture: ComponentFixture<FileAsserts2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FileAsserts2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileAsserts2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
