import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { includes } from "lodash";
import { FilesService } from '../../services/files.service';
import { MakeClaimService } from '../../services/make-claim.service';
import { Web3Service } from '../../services/web3.service';

@Component({
  selector: 'app-file-asserts2',
  templateUrl: './file-asserts2.component.html',
  styleUrls: ['./file-asserts2.component.css']
})
export class FileAsserts2Component implements OnInit {
  claims = <any[]>[];
  fileExtension: string = "pdf";
  checksum: string = "loading";
  hasSigningPending: boolean = false;
  newClaim = {
    address: "",
    content: "",
    froms: <string[]>[],
  };
  myAddress: string;
  loading: boolean = true;
  registered: boolean = true;

  constructor(private route: ActivatedRoute, private filesService: FilesService, private web3Service: Web3Service) { }

  ngOnInit() {
    this.web3Service.address.subscribe((address) => {this.myAddress = address});

    this.route.paramMap.subscribe(async (params) => {
      const fileExtension = params.get("fileExtension");
      const checksum = params.get("checksum");

      if (!fileExtension || !checksum) {
        alert("Checksum or fileExtension is falsy. Variables not updated");
        throw new Error("Checksum or fileExtension is falsy. Variables not updated");
      }

      this.loading = true;
      this.claims = [];

      this.fileExtension = fileExtension;
      this.checksum = checksum;
  
      for await (const claim of this.filesService.getClaimsByKey(this.checksum, this.fileExtension)) {
        // claim.froms = claim.froms.map((from: any) => Object.assign(from, {link: `/registry2/users/${from.address}`}));
        console.log("claim", claim);
        this.claims.push(claim);
      }

      this.loading = false;
    });
  }

  async initClaimInstance() {
    if (!this.newClaim.address) {
      this.newClaim.address = await this.filesService.createClaim(this.checksum, this.fileExtension, this.newClaim.content);
      this.newClaim.froms.push(this.myAddress);
    }
  }

  async addCosign() {
    this.hasSigningPending = true;

    await this.initClaimInstance();
    if (!includes(this.newClaim.froms, this.myAddress)) {
      await this.filesService.cosign(this.newClaim.address);
      this.newClaim.froms.push(this.myAddress);
    }

    this.hasSigningPending = false;
  }

  async doneSign() {
    await this.addCosign();
    window.location.reload();
  }

}
