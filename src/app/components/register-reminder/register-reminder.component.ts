import { Component, OnInit } from '@angular/core';
import { Web3Service } from '../../services/web3.service';
import { RegistryService } from '../../services/registry.service';

@Component({
  selector: 'app-register-reminder',
  templateUrl: './register-reminder.component.html',
  styleUrls: ['./register-reminder.component.css']
})
export class RegisterReminderComponent implements OnInit {
  registered: boolean = true;

  constructor(private web3Service: Web3Service, private registryService: RegistryService) { }

  ngOnInit() {
    this.web3Service.address.subscribe(async (address) => {
      console.log("address from reminder", address);
      this.registered = !!await this.registryService.getName(address);
    });
  }

}
