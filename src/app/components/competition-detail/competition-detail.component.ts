import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ViewCompetitionService } from '../../services/view-competition.service';
import { Web3Service } from '../../services/web3.service';

@Component({
  selector: 'app-competition-detail',
  templateUrl: './competition-detail.component.html',
  styleUrls: ['./competition-detail.component.css']
})
export class CompetitionDetailComponent implements OnInit {
  contestants: any[] = [];
  description: string = "";
  loading: boolean = true;
  myAddress: string;
  voted: boolean | undefined = undefined;

  constructor(private route: ActivatedRoute, private viewCompetitionService: ViewCompetitionService, private web3Service: Web3Service) {}

  ngOnInit() {
    this.web3Service.address.subscribe((address) => {
      this.myAddress = address;
    });

    this.route.paramMap.subscribe(async (params) => {
      const competitionRegistry = params.get("competitionAddress");
      if (!competitionRegistry) {
        return console.error("returning because cannot get competitionRegistry", competitionRegistry);
      }

      this.viewCompetitionService.getDescription(competitionRegistry).then((description) => {
        this.description = description;
      });

      this.viewCompetitionService.alreadyVoted(competitionRegistry, this.myAddress).then((voted) => {
        this.voted = voted;
      });

      for await (const contestant of this.viewCompetitionService.getContestants(competitionRegistry)) {
        this.contestants.push(contestant);
      }
      this.loading = false;
    });
  }

  async voteUp(contestant: any) {
    this.loading = true;
    await this.viewCompetitionService.voteUp(contestant.address);
    window.location.reload();
  }

}
