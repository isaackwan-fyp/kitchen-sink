import { Injectable } from '@angular/core';
import { Web3Service } from './web3.service';
import { COMPETITIONMOTHER as CONSTANTS, COMPETITIONREGISTRY as CONSTANTS2 } from './constants';
import { promisify } from "es6-promisify";
import * as BigNumber from "BigNumber";

//(Symbol as any).asyncIterator = Symbol.asyncIterator || Symbol.for("Symbol.asyncIterator")

function getRandomArbitrary(min: number, max: number) {
    return Math.round(Math.random() * (max - min) + min);
}

@Injectable()
export class MakeCompetitionService {
	private contract: any;
	private address: string;
	private web3: any;

	constructor(web3Service: Web3Service) {
		web3Service.instance.subscribe((web3) => {
			this.contract = web3.eth.contract(CONSTANTS.ABI).at(CONSTANTS.ADDRESS);
			this.web3 = web3;
		});
		web3Service.address.subscribe((address) => {
			this.address = address;
		});
	}

	private addContestant(competitionRegistryAddress: string, candidate: any) {
		const contract = this.web3.eth.contract(CONSTANTS2.ABI).at(competitionRegistryAddress);
		const nonce = getRandomArbitrary(-2147483647, 2147483646);
		return new Promise<string>((resolve, reject) => {
			const event = contract.Claimed([{_from: this.address, nonce: nonce}]);
			event.watch((error: any, result: any) => {
				if (error) {
					return reject(error);
				}
				console.info('Claimed event', result);
				//console.info('new registry-ID', result.args.id.toString());
				if (result.args.nonce.comparedTo(nonce) === 0) {
					event.stopWatching(function(){});
					resolve(result.args.claim);
				} else {
					console.info("Claimed event ignored because nonce did not match", result);
				}
			});
			contract.claim(candidate.file, candidate.description, nonce, {gas: 1100000}, function (error: any, info: any){
				if (error) {
					return reject(error);
				}
				console.log("Claim TX", info);
			});
		})
	}

	/**
	 * @returns {Promise<string>} Address of the CompetitionRegistry instance
	 */
	async create(description: string, candidates: Array<any> = [], value: Number = 0) {
		const competitionAddress = await new Promise<string>((resolve, reject) => {
			const event = this.contract.StartedCompetition([{_from: this.address}]);
			event.watch((error: any, result: any) => {
				if (error) {
					return reject(error);
				}
				console.info('StartedCompetition event', result);
				//console.info('new registry-ID', result.args.id.toString());
				event.stopWatching(function(){});
				resolve(result.args.registry);
			});
			this.contract.create(description, {gas: 1956550, value: value}, function (error: any, tx: any) {
				if (error) {
					return reject(error);
				}
			});
		});
		const adds = Promise.all(candidates.map((candidate) => this.addContestant(competitionAddress, candidate)));
		console.log(adds, candidates);
		await adds;
		return competitionAddress;
	}

	async *getCompetitionAddresses(): AsyncIterableIterator<string> {
		const length = <any>await promisify(this.contract.competitionsLength)();
		for (let i = 0; i < length.toNumber(); i++) {
			const val: string = await promisify(this.contract.competitions)(i);
			yield val;
		}
	}
}
