import { TestBed, inject } from '@angular/core/testing';

import { MakeClaimService } from './make-claim.service';

describe('MakeClaimService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MakeClaimService]
    });
  });

  it('should be created', inject([MakeClaimService], (service: MakeClaimService) => {
    expect(service).toBeTruthy();
  }));
});
