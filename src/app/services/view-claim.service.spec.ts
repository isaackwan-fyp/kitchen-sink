import { TestBed, inject } from '@angular/core/testing';

import { ViewClaimService } from './view-claim.service';

describe('ViewClaimService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ViewClaimService]
    });
  });

  it('should be created', inject([ViewClaimService], (service: ViewClaimService) => {
    expect(service).toBeTruthy();
  }));
});
