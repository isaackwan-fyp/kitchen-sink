import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { Web3Service } from './web3.service';
import { RegistryService } from './registry.service';
import { CLAIM2 as CONSTANTS } from './constants';
import { promisify } from "es6-promisify";
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ViewClaimService {
	private contractClass: any;
	private address: string;
	
	constructor(web3Service: Web3Service, private registryService: RegistryService) {
		web3Service.instance.subscribe((web3) => {
			this.contractClass = web3.eth.contract(CONSTANTS.ABI);
		});
		web3Service.address.subscribe((address) => {
			this.address = address;
		});
	}

	async *getSignedByAddresses(contract: any) {
		// yield promisify(contract.by)();
		const length = <any>await promisify(contract.votedUpLength)();
		for (let i = 0; i < length.toNumber(); i++) {
			yield promisify(contract.votedUp)(i);
		}
	}
	
	async getClaimByAddress(address: string): Promise<any> {
		const contract = this.contractClass.at(address);
		const claim = await Promise.all([promisify(contract.assertion)(), promisify(contract.key)(), promisify(contract.timestamp)()])
							.then(([content, key, timestamp]) => ({
								content: content,
								key: key,
								timestamp: new Date(1000 * parseInt(timestamp, 10)),
								froms: <string[]>[],
								address: address,
							}))
		for await (const person of this.getSignedByAddresses(contract)) {
			claim.froms.push(person);
		}
		return claim;
	}
	
	async voteUp(address: string) {
		const contract: any = this.contractClass.at(address);
		const promise = new Promise<void>((resolve, reject) => {
			const event1 = contract.Paid([{_to: this.address}]);
			const event2 = contract.VoteRejected([{_from: this.address}]);
			event1.watch((error: any, result: any) => {
				if (error) {
					return reject(error);
				}
				event1.stopWatching(function(){});
				event2.stopWatching(function(){});
				resolve(result.args.value.toString());
			});
			event2.watch((error: any, result: any) => {
				if (error) {
					return reject(error);
				}
				event1.stopWatching(function(){});
				event2.stopWatching(function(){});
				reject(new Error("You already voted"));
			});
			contract.voteUp(function(error: any, info: any){
				if (error) {
					return reject(error);
				}
				console.log("voteUp TX", info);
			});
		});
		return promise;
	}

	async voteDown(address: string) {
		const contract: any = this.contractClass.at(address);
		const promise = new Promise<void>((resolve, reject) => {
			const event1 = contract.Paid([{_to: this.address}]);
			const event2 = contract.VoteRejected([{_from: this.address}]);
			event1.watch((error: any, result: any) => {
				if (error) {
					return reject(error);
				}
				event1.stopWatching(function(){});
				event2.stopWatching(function(){});
				resolve(result.args.value.toString());
			});
			event2.watch((error: any, result: any) => {
				if (error) {
					return reject(error);
				}
				event1.stopWatching(function(){});
				event2.stopWatching(function(){});
				reject(new Error("You already voted"));
			});
			contract.voteDown(function(error: any, info: any){
				if (error) {
					return reject(error);
				}
				console.log("voteDown TX", info);
			});
		});
		return promise;
	}

}
