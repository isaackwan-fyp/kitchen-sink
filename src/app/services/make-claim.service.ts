import { Injectable } from '@angular/core';
import { Web3Service } from './web3.service';
import { MAKECLAIM2 as CONSTANTS } from './constants';
import { promisify } from "es6-promisify";

//(Symbol as any).asyncIterator = Symbol.asyncIterator || Symbol.for("Symbol.asyncIterator")

@Injectable()
export class MakeClaimService {
	private contract: any;
	private address: string;

	constructor(web3Service: Web3Service) {
		web3Service.instance.subscribe((web3) => {
			this.contract = web3.eth.contract(CONSTANTS.ABI).at(CONSTANTS.ADDRESS);
		});
		web3Service.address.subscribe((address) => {
			this.address = address;
		});
	}

	/**
	 * @returns {Promise<string>} Address of the Claim instance
	 */
	claim(claim: string, key: string, value: Number) {
		return new Promise<string>((resolve, reject) => {
			const event = this.contract.Claimed([{_from: this.address}]);
			event.watch((error: any, result: any) => {
				if (error) {
					return reject(error);
				}
				console.info('Claimed event', result);
				//console.info('new registry-ID', result.args.id.toString());
				event.stopWatching(function(){});
				resolve(result.args.claim);
			});
			this.contract.claim(claim, key, {gas: 1100000, value: value}, function (error: any, info: any){
				if (error) {
					return reject(error);
				}
				console.log("Claim TX", info);
			});
		})
	}

	async *getClaims() {
		const length = <any>await promisify(this.contract.claimsLength)();
		console.log("length", length, length.toNumber());
		for (let i = 0; i < length.toNumber(); i++) {
			const val: string = await promisify(this.contract.claims)(i);
			yield val;
		}
	}

	async *getClaimAddressesByKey(key: string): AsyncIterableIterator<any> {
		const len = await promisify(this.contract.getClaimsLengthByKey)(key);
		for (let i = 0; i < len; i++) {
			yield promisify(this.contract.getClaimAddressByKeyAndIndex)(key, i);
		}
	}

	async *getClaimAddressesByUser(address: string): AsyncIterableIterator<any> {
		const len = await promisify(this.contract.getClaimsLengthByUser)(address);
		for (let i = 0; i < len; i++) {
			yield promisify(this.contract.getClaimAddressByUserAndIndex)(address, i);
		}
	}
}
