import { Injectable } from '@angular/core';
import { Web3Service } from './web3.service';
import { CLAIM2, MAKECLAIM2, REGISTRY2 } from './constants';
import { promisify } from "es6-promisify";
import { MakeClaimService } from "../services/make-claim.service";
import { ViewClaimService } from "../services/view-claim.service";
import { RegistryService } from './registry.service';

@Injectable()
export class FilesService {
	private address: string;

	constructor(private web3Service: Web3Service, private makeClaimService: MakeClaimService, private viewClaimService: ViewClaimService, private registryService: RegistryService) {
		web3Service.address.subscribe((address) => {
			this.address = address;
		});
	}

	static fileNameToKey(checksum: string, fileExtension: string) : string {
		return JSON.stringify({checksum: checksum, fileExtension: fileExtension});
	}

	async *getClaimsByKey(checksum: string, fileExtension: string) {
		const addresses: AsyncIterableIterator<any> = await this.makeClaimService.getClaimAddressesByKey(FilesService.fileNameToKey(checksum, fileExtension));
		for await (const address of addresses) {
			const claim = await this.viewClaimService.getClaimByAddress(address);
			yield claim;
		}
	}

	async *getClaimsByUser(address: string) {
		const addresses: AsyncIterableIterator<any> = await this.makeClaimService.getClaimAddressesByUser(address);
		for await (const address of addresses) {
			const claim = await this.viewClaimService.getClaimByAddress(address);
			yield claim;
		}
	}

	/**
	 * @return {string} the address of the Claim instance
	 */
	async createClaim(checksum: string, fileExtension: string, content: string): Promise<string> {
		const claim = await this.makeClaimService.claim(content, FilesService.fileNameToKey(checksum, fileExtension), 0);
		await this.viewClaimService.voteUp(claim);
		return claim;
	}

	/**
	 * @return {Promise<void>} a promise that resolves when the co-sign is recorded
	 */
	cosign(address: string): Promise<void>{
		return this.viewClaimService.voteUp(address);
	}
}
