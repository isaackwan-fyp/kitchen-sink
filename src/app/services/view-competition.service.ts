import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { Web3Service } from './web3.service';
import { COMPETITIONREGISTRY as CONSTANTS, CONTESTANT as CONSTANTS2 } from './constants';
import { promisify } from "es6-promisify";
import { Observable } from 'rxjs/Observable';
import { checkAndUpdatePureExpressionInline, checkAndUpdatePureExpressionDynamic } from '@angular/core/src/view/pure_expression';

function range(n: number) {
	return Array.from(Array(n).keys());
}

@Injectable()
export class ViewCompetitionService {
	private contractClass: any;
	private contestantClass: any;
	private address: string;
	
	constructor(web3Service: Web3Service) {
		web3Service.instance.subscribe((web3) => {
			this.contractClass = web3.eth.contract(CONSTANTS.ABI);
			this.contestantClass = web3.eth.contract(CONSTANTS2.ABI);
		});
		web3Service.address.subscribe((address) => {
			this.address = address;
		});
	}

	async getDescription(address: string): Promise<string> {
		const contract = this.contractClass.at(address);
		return promisify(contract.description)();
	}

	async *getContestants(address: string) {
		// yield promisify(contract.by)();
		const contract = this.contractClass.at(address);
		const length = <any>await promisify(contract.claimsLength)();

		for (let i = 0; i < length; i++) {
			yield promisify(contract.claims)(i).then((addressOfContestant: string) => {
				const contestant = this.contestantClass.at(addressOfContestant);
				const votedBy = promisify(contestant.votedUpLength)().then((length: any) => {
					length = length.toNumber();
					return Promise.all(range(length).map((i) => promisify(contestant.votedUp)(i)));
				});
				return Promise.all([promisify(contestant.assertion)(), promisify(contestant.key)(), votedBy]).then(([file, description, votedBy]) => ({
					address: addressOfContestant,
					file: file,
					description: description,
					votedBy: votedBy,
				}));
			})
		}
	}
	
	
	voteUp(address: string) {
		const contract: any = this.contestantClass.at(address);
		const promise = new Promise<void>((resolve, reject) => {
			const event1 = contract.Paid([{_to: this.address}]);
			const event2 = contract.VoteRejected([{_from: this.address}]);
			event1.watch((error: any, result: any) => {
				if (error) {
					return reject(error);
				}
				event1.stopWatching(function(){});
				event2.stopWatching(function(){});
				resolve(result.args.value.toString());
			});
			event2.watch((error: any, result: any) => {
				if (error) {
					return reject(error);
				}
				event1.stopWatching(function(){});
				event2.stopWatching(function(){});
				reject(new Error("You already voted"));
			});
			contract.voteUp(function(error: any, info: any){
				if (error) {
					return reject(error);
				}
				console.log("voteUp TX", info);
			});
		});
		return promise;
	}

	getCompetitionDetails(address: string) {
		const contract = this.contractClass.at(address);
		return Promise.all([promisify(contract.description)(), promisify(contract.claims)(0)])
			.then(([description, contestant]) => {
				if (contestant === "0x") {
					console.error("No image is found for ", address);
					contestant = "https://s26.postimg.cc/49de3y189/pexels-photo-606453.jpg";
				} else {
					contestant = promisify(this.contestantClass.at(contestant).assertion)()
				}
				return Promise.all([description, contestant]);
			}).then(([description, file]) => {
				return {
					address: address,
					description: description,
					file: file,
				};
			})
	}
	
	alreadyVoted(competitionRegistryAddress: string, userAddress: string) : Promise<boolean> {
		return promisify(this.contractClass.at(competitionRegistryAddress).voted)(userAddress)
				.then((addr: string) => "0x0000000000000000000000000000000000000000" !== addr);
	}

}
