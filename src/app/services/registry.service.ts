import { Injectable } from '@angular/core';
import { REGISTRY2 as CONSTANTS } from './constants';
import User from '../models/User';
import Web3 from 'web3';
import { Web3Service } from './web3.service';
import { promisify } from "es6-promisify";

declare global {	
	interface Window {
		web3: any;
	}
}

@Injectable()
export class RegistryService {
	private contract: any;
	private address: string;
	constructor(web3Service: Web3Service) {
		web3Service.instance.subscribe((web3) => {
			this.contract = web3.eth.contract(CONSTANTS.ABI).at(CONSTANTS.ADDRESS);
		});
		web3Service.address.subscribe((address) => {
			this.address = address;
		});
	}
	
	/**
	 * @returns {Promise<string>} the registry-ID of the new registration
	 */
	register(name: string, avatar: string): Promise<string> {
		return new Promise((resolve, reject) => {
			const event = this.contract.Registerd([{_from: this.address}]);
			event.watch((error: any, result: any) => {
				if (error) {
					return reject(error);
				}
				console.debug('Registered event', result);
				console.info('new registry-ID', result.args.id.toString());
				event.stopWatching(function(){});
				resolve(result.args.id.toString());
			});
			this.contract.register(name, avatar, {gas: "500000"}, function(error: any, info: any){
				if (error) {
					return reject(error);
				}
				console.log("Register TX", info);
			});
		})
	}
	
	friend(address: string): Promise<void> {
		return new Promise((resolve, reject) => {
			const event = this.contract.Friended([{_from: this.address, _to: address}]);
			event.watch((error: any, result: any) => {
				if (error) {
					return reject(error);
				}
				console.debug('Friended event', result);
				event.stopWatching(function(){});
				resolve();
			});
			this.contract.friend(address, {gas: "300000"}, function(error: any, info: any){
				if (error) {
					return reject(error);
				}
				console.log("Friend TX", info);
			});
		})
	}
	
	unfriend(address: string): Promise<void> {
		return new Promise((resolve, reject) => {
			const event = this.contract.Unfriended([{_from: this.address, _to: address}]);
			event.watch((error: any, result: any) => {
				if (error) {
					return reject(error);
				}
				console.debug('Unfriended event', result);
				event.stopWatching(function(){});
				resolve();
			});
			this.contract.unfriend(address, {gas: "300000"}, function(error: any, info: any){
				if (error) {
					return reject(error);
				}
				console.log("Unfriend TX", info);
			});
		})
	}
	
	isFriend(address1: string, address2: string): Promise<boolean> {
		return new Promise((resolve, reject) => {
			this.contract.isFriend(address1, address2, (error: any, result: any) => {
				if (error) {
					return reject(error);
				}
				resolve(result);
			});
		})
	}
	
	pagerank(address: string): Promise<any> {
		return new Promise((resolve, reject) => {
			this.contract.pagerank(address, {gas: "6770000"}, (error: any, result: any) => {
				if (error) {
					return reject(error);
				}
				resolve(result);
			});
		})
	}

	getName(address: string): Promise<string> {
		return promisify(this.contract.names)(address, {gas: "500000"});
	}

	getAvatar(address: string): Promise<string> {
		return promisify(this.contract.avatars)(address, {gas: "500000"});
	}
}
