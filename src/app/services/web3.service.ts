import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject'
import * as Web3 from 'web3';
import { Connect, MNID } from 'uport-connect';

@Injectable()
export class Web3Service {
	public instance: BehaviorSubject<any>;
	public address: BehaviorSubject<string>;
	private normal: any;
	private uport?: any;
	private uport_address?: any;
	constructor() {
		this.instance = new BehaviorSubject(undefined);
		this.address = new BehaviorSubject("garbage");
		this.switchToNormal();
	}

	async switchToUport(): Promise<any> {
		if (this.uport == null) {
			const connect = new Connect('WOB', {
				network: {
					id: '0x4',
					registry: '0x2cc31912b2b0f3075a87b3640923d45a26cef3ee',
					rpcUrl: 'http://localhost:8545'
				},
				// network: 'rinkeby',
			});
			const address = await connect.requestCredentials({notifcations: true}).then((profile: any) => {
				const decoded = MNID.decode(profile.address);
				console.log("User profile from uPort", profile, decoded);
				return decoded.address;
			});
			this.uport_address = address;
			//this.uport = connect.getWeb3();
			const address_backup = connect.getWeb3().eth.defaultAccount;
			this.uport = new Web3(connect.getWeb3().currentProvider);
			this.uport.eth.defaultAccount = address_backup;
			console.log(address_backup, address);
		}
		this.instance.next(this.uport);
		this.address.next(this.uport_address);
		console.log("Switched to uPort", this.uport);
		return this.uport;
	}

	switchToNormal(): any {
		if (typeof window !== 'undefined' && typeof window.web3 !== 'undefined') {
			this.normal = new Web3(window.web3.currentProvider);
			this.normal.eth.defaultAccount = window.web3.eth.defaultAccount;
		} else {
			this.normal = new Web3(new Web3.providers.HttpProvider('http://localhost:8545'));
		}
		if (!this.normal.eth.defaultAccount) {
			if (typeof window !== 'undefined') {
				this.normal.eth.defaultAccount = window.prompt('Enter default account address (starts with 0x)');				
			} else {
				throw new Error('Default Account not specified');
			}
		}
		this.instance.next(this.normal);
		this.address.next(this.normal.eth.defaultAccount);
		console.log("Switched to Normal", this.normal);
		return this.normal;
	}

}
