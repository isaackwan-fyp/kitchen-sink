import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { QRCodeModule } from 'angular2-qrcode';
import { HttpClientModule } from "@angular/common/http";


import { RegistryService } from './services/registry.service';
import { Web3Service } from './services/web3.service';
import { MakeClaimService } from './services/make-claim.service';
import { MakeCompetitionService } from './services/make-competition.service';
import { ViewClaimService } from './services/view-claim.service';
import { ViewCompetitionService } from './services/view-competition.service';
import { FilesService } from './services/files.service';

import { AuthenticationStateComponent } from './components/authentication-state/authentication-state.component';
import { RegistryMutationsComponent } from './components/registry-mutations/registry-mutations.component';
import { RegistryRegisterComponent } from './components/registry-register/registry-register.component';
import { AppRoutingModule } from './app-routing.module';
import { WelcomePageComponent } from './components/welcome-page/welcome-page.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ClaimMakeComponent } from './components/claim-make/claim-make.component';
import { ClaimViewComponent } from './components/claim-view/claim-view.component';
import { AssertPageComponent } from './components/assert-page/assert-page.component';
import { VerifyPageComponent } from './components/verify-page/verify-page.component';
import { FileAssertsComponent } from './components/file-asserts/file-asserts.component';
import { FileAsserts2Component } from './components/file-asserts2/file-asserts2.component';
import { FileNewComponent } from './components/file-new/file-new.component';
import { UserBadgeComponent } from './components/user-badge/user-badge.component';
import { Asserts2OfUserComponent } from './components/asserts2-of-user/asserts2-of-user.component';
import { RegisterReminderComponent } from './components/register-reminder/register-reminder.component';
import { VoteCreateComponent } from './components/vote-create/vote-create.component';
import { CompetitionDetailComponent } from './components/competition-detail/competition-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    WelcomePageComponent,
    RegistryMutationsComponent,
    AuthenticationStateComponent,
    ClaimMakeComponent,
    ClaimViewComponent,
    AssertPageComponent,
    VerifyPageComponent,
    FileAssertsComponent,
    FileAsserts2Component,
    FileNewComponent,
    UserBadgeComponent,
    Asserts2OfUserComponent,
    RegisterReminderComponent,
    RegistryRegisterComponent,
    VoteCreateComponent,
    CompetitionDetailComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    NgbModule.forRoot(),
    QRCodeModule,
    HttpClientModule,
  ],
  providers: [RegistryService, Web3Service, MakeClaimService, MakeCompetitionService, ViewClaimService, ViewCompetitionService, FilesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
