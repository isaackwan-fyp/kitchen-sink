/* SystemJS module definition */
declare var module: NodeModule;
interface NodeModule {
  id: string;
}

declare module 'uport-connect' {
  export let Connect: any;
  export let MNID: any;
}

declare module 'web3';
declare module 'es6-promisify';
declare module 'js-sha1';
declare module 'BigNumber';
// declare module 'bignumber.js';